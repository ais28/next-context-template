# NextJS App Context Template

Este proyecto tiene como finalidad servir como un template para iniciar un nuevo proyecto en Next.js o para proporcionar ejemplos claros sobre el uso de contextos en React.

## Características

En este proyecto encontrarás:

### `useContext`

`useContext` es un hook de React que permite acceder a valores en un contexto desde cualquier componente funcional sin necesidad de pasar props manualmente a través del árbol de componentes.

## Uso del Proyecto

### Instalación

Para comenzar con este template, primero clona el repositorio y luego instala las dependencias necesarias.

```bash
git clone next-context-template
cd next-context-template
npm install
# o
yarn
